import React from 'react';
import { View, Image, Text } from 'react-native';
import { global, view } from '../styles/styles';


export default function kittenView({ navigation }){

	const name = navigation.getParam('name');
	const img = navigation.getParam('img');
	const description = navigation.getParam('description');

	return(
		<View style = {global.flex}>
			<Image
				source = {{ uri: (img) }}
				style = {view.image}
				resizeMode = "contain"
			/>
			<Text style = {[global.title, view.title]}>{ name }</Text>
			<Text style = {view.description}>{ description }</Text>
		</View>
	);
}

