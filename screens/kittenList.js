import React, { useState, useEffect } from 'react';
import { View, FlatList, Modal, Button, ActivityIndicator } from 'react-native';
import Filters from '../components/filters';
import Card from '../components/card';
import { global } from '../styles/styles';

export default function KittenList({ navigation }) {

	const nameList = [ "Tom", "Jerry", "Garfield", "Ridle", "Onnion", "Ruddie", "Pumpkin", "Mangas", "Braske", "Rikis" ];
	const [modalOpen, setModalOpen] = useState(false);
	const [isLoading, setIsLoading] = useState(false);
	const [catList, setCatList] = useState([]);
	const [count, setCount] = useState(0);


	const randomNumber = () => {
		return Math.floor(Math.random() * 16) + 1;
	}


	const prepareCatList = () => {
		setCatList([]);
		var tempCatList = [];
		for(var i = 0; i < count; i++){
			tempCatList.push({
				name: nameList[Math.floor(Math.random() * nameList.length)],
				img: 'https://placekitten.com/300/200?image='+randomNumber(),
				key: i.toString(),
				description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.'
			});
		};
		
		setCatList(oldCatList => [...oldCatList, ...tempCatList]);
	};
	
	const switchFilter = (num) => {
		setCount(num);
		closeModal();
		setIsLoading(true);
	};

	useEffect(() => {
		if(isLoading) prepareCatList();
	}, [isLoading]);

	useEffect(() =>{
		if(catList.length == count) setIsLoading(false);
	}, [catList])

	const closeModal = () => {
		setModalOpen(false);
	};

	const viewItem = (item) => {
		navigation.navigate('KittenView', item);
	};


	return (
		<View style = {global.flex}>
			
			<Modal visible={modalOpen} animationType='fade'>
				<View>
					<Filters switchFilter={switchFilter} closeModal={closeModal} />
				</View>
			</Modal>
			
			{isLoading && 
				<View style={global.loading}>
					<ActivityIndicator size='large' color="#0000ff" />
				</View>
			}

			<Button title='Filter' onPress={() => {setModalOpen(true)}} />
			<FlatList 
				style = {global.flex}
				data = {catList}
				renderItem = {({ item }) => (
					<Card viewItem={viewItem} item={item} />
				)}
			/>
		</View>
	);
}