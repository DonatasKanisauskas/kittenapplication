import { createStackNavigator } from 'react-navigation-stack';
import { createAppContainer } from 'react-navigation';
import KittenList from '../screens/kittenList';
import KittenView from '../screens/kittenView';

const screens = {
    KittenList: {
        screen: KittenList,
        navigationOptions: {
            title: 'Kitten list'
        }
    },
    KittenView: {
        screen: KittenView,
        navigationOptions: {
            title: 'Kitten view'
        }
    },
}

const HomeStack = createStackNavigator(screens, {
    defaultNavigationOptions: {
        headerStyle: { 
            backgroundColor: '#00ff03',
            borderBottomWidth: 1,
            borderBottomColor: 'black',
        },
        headerTitleStyle: { alignSelf: 'center' },
    }
});

export default createAppContainer(HomeStack);