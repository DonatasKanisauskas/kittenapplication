# KittenApplication

This is a simple react-native kitten application using placekitten.com API. It retrieves images from kitten API and incorporates those images into mobile screen.

___

## 📲 Major Screens 

* Kitten List
* Kitten View

`Kitten list` displays `cards`, which leads to `Kitten view` with specific kitten description. 
___

## 📦 Components

* Card
* Filters

Changing `filter` value changes number of `cards` displayed in `kitten list`.
___

## 🔧 Built With

*  JavaScript
* React Native
___

## ⚙️ Installation 

```sh
npm install expo
```
```sh
npm start
# or 
expo start
```
